# -*- mode: makefile-gmake; coding: utf-8 -*-

DESTDIR?=~

SCONEDIR=/usr/share/scone
BASE=$(DESTDIR)$(SCONEDIR)
DOCDIR=$(DESTDIR)/usr/share/doc/scone

all:

install:
	install -d $(BASE)
	install -d $(BASE)/kb
	install -d $(BASE)/kb/core-components
	install -d $(BASE)/kb/lexical-components

	install -v -m 444 scone/*.lisp $(BASE)/
	install -v -m 444 scone/kb/*.lisp $(BASE)/kb
	install -v -m 444 scone/kb/core-components/* $(BASE)/kb/core-components
	install -v -m 444 scone/kb/lexical-components/* $(BASE)/kb/lexical-components

	install -d $(DOCDIR)
	install -v -m 555 scone/Scone-Users-Guide.pdf $(DOCDIR)
